#include <cmath>

#include "datum.h"

Datum::Datum(double val)
  : value(val)
  , variance(0)
{}

Datum::Datum(double val, Stdev stdev)
  : value(val)
  , variance(stdev.value * stdev.value)
{}

Datum::Datum(double val, Variance var)
  : value(val)
  , variance(var.value)
{}

Datum Datum::operator+(Datum const& op2) const {
  return Datum {
    value + op2.value,
    Variance { variance + op2.variance },
  };
}

Datum Datum::operator-(Datum const& op2) const {
  return Datum {
    value - op2.value,
    Variance { variance + op2.variance },
  };
}

Datum Datum::operator*(Datum const& op2) const {
  return Datum {
    value * op2.value,
    Variance { variance * op2.value * op2.value + value * value * op2.variance },
  };
}

Datum Datum::operator/(Datum const& op2) const {
  auto value2 = value * value;
  auto op2_value2 = op2.value * op2.value;
  auto op2_value4 = op2_value2 * op2_value2;
  return Datum {
    value / op2.value,
    Variance { variance / op2_value2 + op2.variance * value2 / op2_value4 },
  };
}

namespace std {
Datum log(Datum const& d) {
  return Datum {
    std::log(d.value),
    Datum::Variance { d.variance / (d.value * d.value) },
  };
}
}
