#include <iostream>
#include <cmath>

#include "datum.h"

int main () {
  double milliohms_measured = 33.0;
  auto milliohms = Datum {milliohms_measured, Datum::Stdev {milliohms_measured * 0.00008 + 100000 * 0.00004}};
  auto length = Datum(23, Datum::Stdev{1});
  auto inches_per_foot = Datum(12, Datum::Stdev{0});
  auto resistance_per_length = milliohms / length * inches_per_foot;
  std::cout << resistance_per_length.value << std::endl;
  std::cout << std::sqrt(resistance_per_length.variance) << std::endl;
}
