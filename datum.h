#ifndef DATUM_H
#define DATUM_H

struct Datum {
  double value;
  double variance;

  struct Stdev {
    double value;
  };
  struct Variance {
    double value;
  };

  Datum(double);
  Datum(double, Stdev);
  Datum(double, Variance);

  Datum operator+(Datum const&) const;
  Datum operator-(Datum const&) const;
  Datum operator*(Datum const&) const;
  Datum operator/(Datum const&) const;
};

namespace std {
Datum log(Datum const&);
}

#endif
